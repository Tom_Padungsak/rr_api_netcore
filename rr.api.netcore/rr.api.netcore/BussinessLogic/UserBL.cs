﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using rr.api.netcore.BussinessLogic;
using rr.api.netcore.Configurations;
using rr.api.netcore.Helpers.Enums;
using rr.api.netcore.Https.Authenticate;
using rr.api.netcore.Https.Response;
using rr.api.netcore.Https.Responses;
using rr.api.netcore.IServices;
using rr.api.netcore.Model;
using rr.api.netcore.Model.DataAccesses;
using rr.api.netcore.Security;
using RRIServiceModel.Models;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;

namespace rr.api.netcore.Services
{
    public class UserBL : AbBLRepository
    {
        public ApiResponse Authenticate(AuthenticateRequest model)
        {
            MtUser mtUser = new MtUser();
            JWTManagement jWTManagement = new JWTManagement();
            mtUser.Data = mtUser.Authenticate(model);
            if (mtUser.Data == null) return new ApiResponse() { status = new StatusResponse() { code = (int)BusinessStatusCode.InvalidAccessRights }, data = "Username or password is incorrect" };

            UserAccessModel user = new UserAccessModel()
            {
                fullName = mtUser.Data.NAME,
                firstName = mtUser.Data.NAME,
                lastName = "",
                password = mtUser.Data.PASSWORD,
                userId = mtUser.Data.USER_ID,
                username = mtUser.Data.USERNAME
            };

            var token = jWTManagement.GenerateJwtToken(JsonConvert.SerializeObject(user));
            ApiResponse.data = new AuthenticateResponse(user, token);
            return ApiResponse;
        }

        public ApiResponse GetUserList()
        {
            MtUser mtUser = new MtUser();

            mtUser.Datas = mtUser.GetAll();
            if (mtUser.Datas.ToList().Count > 0)
            {
                ApiResponse.data = mtUser.Datas;
            }
            else
            {
                ApiResponse.status.code = (int)BusinessStatusCode.NoContent;
            }
            return ApiResponse;
        }

        public ApiResponse GetUserByUserId(GetUserByUserIdRequest request)
        {
            MtUser mtUser = new MtUser();
            mtUser.Data = mtUser.GetById(request.userId);
            this.ApiResponse.data = mtUser.Data;
            return this.ApiResponse;
        }


        public ApiResponse SaveUser(SaveUserRequest request)
        {
            MtUser mtUser = new MtUser();
            SaveUserResponse saveUserResponse = new SaveUserResponse();

            var dataSave = mtUser.Save(request,this.UserAccess);

            string serializeDataSave = JsonConvert.SerializeObject(dataSave);
            saveUserResponse = JsonConvert.DeserializeObject<SaveUserResponse>(serializeDataSave);
            saveUserResponse.userId = dataSave.USER_ID;
            saveUserResponse.statusSave = "Done";
            this.ApiResponse.data = saveUserResponse;
            return this.ApiResponse;
        }

        public ApiResponse SaveUserAndStaff(SaveUserAndStaffRequest request)
        {
            MtUser mtUser = new MtUser();
            TrStaffTeam trStaffTeam = new TrStaffTeam();

            SaveUserResponse saveUserResponse = new SaveUserResponse();


            this.DbContextTransactionStart();

            try
            {

                mtUser.DbContext = this.DbContext;
                trStaffTeam.DbContext = this.DbContext;

                var dataSave = mtUser.Save(request,this.UserAccess);

                trStaffTeamDto trStaffTeamDto = new trStaffTeamDto();
                trStaffTeamDto.parentId = trStaffTeamDto.parentId;
                trStaffTeamDto.staffTeamId = trStaffTeamDto.staffTeamId;
                trStaffTeamDto.userId = dataSave.USER_ID;
                trStaffTeamDto.userName = dataSave.USERNAME;

                var saveCom = trStaffTeam.Save(trStaffTeamDto);

                this.DbContextTransactionCommit();
                this.Dispose();
            }
            catch (Exception ex)
            {
                this.DbContextTransactionRollback();
                throw new Exception(ex.Message,ex.InnerException);
            }
           

            this.ApiResponse.data = saveUserResponse;
            return this.ApiResponse;
        }
    }
}
