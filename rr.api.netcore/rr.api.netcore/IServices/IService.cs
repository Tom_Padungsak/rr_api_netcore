﻿using RRIServiceModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.IServices
{
    public interface IService : IDisposable
    {
        AppModelDbContext DbContext { get; }
        void DbContextTransactionRollback();
        void DbContextTransactionCommit();
    }
}
