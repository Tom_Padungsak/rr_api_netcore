﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using rr.api.netcore.Configurations;
using rr.api.netcore.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace rr.api.netcore.Middlewares
{
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;
        private AppSettings appSettings;
        public AuthenticationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IConfiguration configuration)
        {
            try
            {
                appSettings = new AppSettings();
                configuration.GetSection("AppSettings").Bind(appSettings);
                JWTManagement jWTManagement = new JWTManagement();

                string authHeader = context.Request.Headers["Authorization"];

                if (authHeader != null)
                {
                    //Reading the JWT middle part           

                    var token = jWTManagement.Decode(authHeader.Replace("Bearer ", string.Empty));
                    var stringUuserAccess = JsonConvert.SerializeObject(token.Payload.FirstOrDefault().Value);
                    // Identity Principal
                    var claims = new[]
                    {
                       new Claim(ClaimTypes.Name, stringUuserAccess),
                       //new Claim(ClaimTypes.Role, userRule),
                    };
                    var identity = new ClaimsIdentity(claims);
                    context.User = new ClaimsPrincipal(identity);

                    await _next(context);
                }
                else
                {
                    /// return 401
                }
                //Pass to the next middleware
                await _next(context);
            }
            catch (Exception ex)
            {
                //await HandleExceptionAsync(context, ex);
            }
        }
    }
}
