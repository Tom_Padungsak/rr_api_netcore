﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.Https.Authenticate
{
    public class AuthenticateResponse : UserAccessModel
    {
        public TokenAccessModel token { get; set; }
        public AuthenticateResponse(UserAccessModel user, TokenAccessModel token)
        {
            userId = user.userId;
            firstName = user.firstName;
            lastName = user.lastName;
            username = user.username;
            this.token = token;
        }
    }
}
