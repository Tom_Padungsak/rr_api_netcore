﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FireSharp;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using rr.api.netcore.Configurations;
using rr.api.netcore.Model.FirebaseDataAccess;
using rr.api.netcore.Services;
using RRIServiceModel.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace rr.api.netcore.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class PersonController : RRCoreController
    {
        //public PersonController(AppModelDbContext appModelDbContext, IOptions<AppSettings> appSettings) : base(appModelDbContext, appSettings)
        //{
        //}

        public override string ModuleName => "Person";

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetPerson()
        {

            //var a = QueryBuilder.New().OrderBy("firstName");
            
            UserFbService firebaseService = new UserFbService();
            var result = await firebaseService.GetPersonAsync("users");
            return await ToActionResultAsync(result);
        }

        // POST api/<controller>
        [AllowAnonymous]
        [HttpPost("set-person")]
        public async Task<IActionResult> PostPerson(Person value)
        {
            UserFbService firebaseService = new UserFbService();
            var result = await firebaseService.SetPersonAsync("users", value);
            return await ToActionResultAsync(result);
        }
    }
}
