﻿using Microsoft.EntityFrameworkCore.Scaffolding.Metadata;
using rr.api.netcore.Https.Authenticate;
using RRIServiceModel.Models;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.Model.DataAccesses
{
    public partial class MtUser : AbRepository<MT_USER>
    {
        public override MT_USER GetById(string id)
        {
            return this.FindBy(a => a.USER_ID.ToString() == id).FirstOrDefault();
        }

        public override List<MT_USER> GetDatas(string id)
        {
            return this.FindBy(a => a.USER_GROUP_ID.ToString() == id).ToList();
        }

        public MT_USER Authenticate(AuthenticateRequest model)
        {
            return FindBy(r => r.USERNAME == model.Username && r.PASSWORD == model.Password).SingleOrDefault();
        }

        public MT_USER Save(MtUserDto data, UserAccessModel userAccessModel)
        {
            this.Data = this.GetById(data.userId.ToString());
            if (this.Data == null)
            {
                this.Data = this.BindObjectToDB(data);
                this.Data.IS_ACTIVE = "Y";
                this.Data.CREATE_BY = "AD";
                this.Data.CREATE_DATE = DateTime.Now;
                this.Create(this.Data);
            }
            else
            {
                this.Data.NAME = data.userName;
                this.Data.AVATAR = data.avatar;
                this.Data.IS_ACTIVE = "Y";
                this.Data.UPDATE_BY = userAccessModel.fullName;
                this.Data.UPDATE_DATE = DateTime.Now;
                this.Edit(this.Data);
            }

            return this.Data;
        }
    }

    public class MtUserDto : MasterModelDto
    {

        public int userId { get; set; }

        [StringLength(60)]
        public string userName { get; set; }

        [StringLength(60)]
        public string password { get; set; }
        [StringLength(100)]
        public string rememberToken { get; set; }
        public int userTypeId { get; set; }
        public int? userGroupId { get; set; }
        [StringLength(10)]
        public string empNo { get; set; }
        [StringLength(10)]
        public string agentNo { get; set; }
        [StringLength(255)]
        public string name { get; set; }
        [StringLength(255)]
        public string avatar { get; set; }
        [StringLength(255)]
        public string email { get; set; }
        [StringLength(1000)]
        public string remark { get; set; }


    }


    public class MasterModelDto
    {
    [Required]
    [StringLength(1)]
    public string isActive { get; set; }
    [Required]
    [StringLength(20)]
    public string createBy { get; set; }
    public DateTime? createDate { get; set; }
    [StringLength(20)]
    public string updateBy { get; set; }
    public DateTime? updateDate { get; set; }
    [StringLength(20)]
    public string deleteBy { get; set; }
    public DateTime? deleteDate { get; set; }
}
}
