﻿using rr.api.netcore.Https.Authenticate;
using RRIServiceModel.Models;
using RRPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.Model.DataAccesses
{
    public partial class TrStaffTeam : AbRepository<TR_STAFF_TEAM>
    {
        public override TR_STAFF_TEAM GetById(string id)
        {
            return this.FindBy(a => a.STAFF_TEAM_ID.ToString() == id).FirstOrDefault();
        }

        public override List<TR_STAFF_TEAM> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public TR_STAFF_TEAM Save(trStaffTeamDto data)
        {
            this.Data = this.GetById(data.userId.ToString());
            if (this.Data == null)
            {
                this.Data = this.BindObjectToDB(data);
                this.Data.IS_ACTIVE = "Y";
            
                this.Create(this.Data);
            }
            else
            {
                this.Data.USER_ID = data.userId;
                this.Data.USER_NAME = data.userName;
                this.Edit(this.Data);
            }

            return this.Data;
        }


    }

    public class trStaffTeamDto : MasterModel
    {
      
        public int staffTeamId { get; set; }
        public int userId { get; set; }
        public int? parentId { get; set; }
        public string userName { get; set; }
    }
}
