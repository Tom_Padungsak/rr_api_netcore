﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Microsoft.Extensions.Configuration;
using rr.api.netcore.IServices;
using rr.api.netcore.Model.Enum;
using rr.api.netcore.Utility;
using RRIServiceModel.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace rr.api.netcore.Model
{
    public abstract class AbRepository<T> : IDataAccess<T> where T : class, new()
    {
        private T data;
        private IEnumerable<T> datas;
        private AppModelDbContext dbContext;

        public AbRepository()
        {
            this.data = new T();
            this.datas = new List<T>();
        }

        public AppModelDbContext DbContext
        {
            get {

                if (this.dbContext == null)
                {
                    var connectionString = Startup.Configuration.GetSection("AppSettings").GetConnectionString("AppDbContext");
                    var optionsBuilder = new DbContextOptionsBuilder<AppModelDbContext>();
                    optionsBuilder.UseNpgsql(connectionString);
                    this.dbContext = new AppModelDbContext(optionsBuilder.Options);
                    return this.dbContext;
                }
                else
                {
                    return this.dbContext;
                }
            
            }
            set { this.dbContext = value; }
        }


        public virtual T Data
        {
            get { return this.data; }
            set { this.data = value; }
        }

        public virtual IEnumerable<T> Datas
        {
            get { return this.datas; }
            set { this.datas = value; }
        }

        protected Type TypeClass
        {
            get { return typeof(T); }
        }


        public virtual bool Delete(string id, T data)
        {
            bool result = false;
            this.Save(data, ActionType.Delete, out result);
            this.dbContext.SaveChanges();
            return result;
        }

        public abstract T GetById(string id);

        public abstract List<T> GetDatas(string id);

        public virtual IEnumerable<T> GetAll()
        {
            IEnumerable<T> query = DbContext.Set<T>().AsEnumerable();
            return query;
        }

        public IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            IEnumerable<T> query = DbContext.Set<T>().Where(predicate).AsEnumerable();
            return query;
        }

        /// <summary>
        /// การจัดการข้อมูลในฐานข้อมูล(insert, update, delete) ต้องทำผ่าน method นี้ และต้องมี id, data, action และ resultStatus ด้วย
        /// </summary>
        /// <param name="id">
        /// id เป็นรหัสของแถวหรือเป็นคีย์หลัก
        /// </param>
        /// <param name="data">
        /// ข้อมูลที่จะบันทึกเข้าสู่ฐานข้อมูล หรือข้อมูลที่จะลบออกจากฐานข้อมูล
        /// </param>
        /// <param name="action">
        /// ประเภทของการจัดการข้อมูลในฐานข้อมูล
        /// - Add
        /// - Edit
        /// - Delete
        /// </param>
        /// <param name="resultStatus">
        /// สถานะการจัดการข้อมูล 
        /// - กำหนดค่า <b>True</b> กรณีทำรายการสำเร็จ
        /// - กำหนดค่า <b>False</b> กรณีอื่นๆ
        /// </param>
        /// <returns></returns>
        private void Save(T data, ActionType action, out bool resultStatus)
        {
            resultStatus = false;
            try
            {
                switch (action)
                {
                    case ActionType.Add:
                        this.dbContext.Set<T>().Add(data);
                        break;
                    case ActionType.Edit:
                        this.dbContext.Entry(data).State = EntityState.Modified;
                        break;
                    case ActionType.Delete:
                        this.dbContext.Set<T>().Remove(data);
                        break;
                }
                resultStatus = true;


                //if (GetEntityErrorChangeTracker() != string.Empty)
                //{
                //    throw new Exception(GetEntityErrorChangeTracker());
                //}

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void SaveRange(IEnumerable<T> data, ActionType action, out bool resultStatus)
        {
            resultStatus = false;
            try
            {
                switch (action)
                {
                    case ActionType.Add:
                        this.dbContext.Set<T>().AddRange(data);
                        break;
                    case ActionType.Edit:
                        this.dbContext.Entry(data).State = EntityState.Modified;
                        break;
                    case ActionType.Delete:
                        this.dbContext.Set<T>().RemoveRange(data);
                        break;
                }
                resultStatus = true;


                if (GetEntityErrorChangeTracker() != string.Empty)
                {
                    throw new Exception(GetEntityErrorChangeTracker());
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private async Task<bool> SaveRangeAsync(IEnumerable<T> data, ActionType action)
        {
            bool resultStatus = false;
            try
            {
                switch (action)
                {
                    case ActionType.Add:
                        await this.dbContext.Set<T>().AddRangeAsync(data);
                        break;
                    case ActionType.Edit:
                        this.dbContext.Entry(data).State = EntityState.Modified;
                        break;
                    case ActionType.Delete:
                        this.dbContext.Set<T>().RemoveRange(data);
                        break;
                }
                resultStatus = true;


                if (GetEntityErrorChangeTracker() != string.Empty)
                {
                    throw new Exception(GetEntityErrorChangeTracker());
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return resultStatus;
        }

        private string GetEntityErrorChangeTracker()
        {
            string resultException = string.Empty;
            int counts = 1;
            foreach (var entry in this.dbContext.ChangeTracker.Entries())
            {

                if (resultException.Length > 0)
                    resultException += Environment.NewLine;

                resultException += string.Format("Entity : {0}. Module : {1} Error Message : {2}. {3}",
                    counts, entry.Entity.GetType().Name, entry.Entity.GetType().Module, entry.State, Environment.NewLine);
                counts++;
            }
            return resultException;
        }

        public virtual T BindObjectToDB(object input)
        {
            T data = new T();

            NameValueCollection form = new NameValueCollection();

            input.GetType().GetProperties().ToList()
                .ForEach(pi => form.Add(pi.Name, pi.GetValue(input, null) == null ? "" : pi.GetValue(input, null).ToString()));

            var props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var prop in props)
            {
                var propType = prop.PropertyType;
                if (propType.IsGenericType && propType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                    propType = new NullableConverter(propType).UnderlyingType;

                object value = null;
                string requestData = form[prop.Name];
                if (requestData != null)
                {
                    if (!string.IsNullOrEmpty(requestData))
                        value = requestData;
                }
                else
                {
                    requestData = form[prop.Name.ToPascalFormat().ToLowerFirstChar()];
                    if (requestData != null)
                    {
                        if (!string.IsNullOrEmpty(requestData))
                            value = requestData;
                    }
                }

                if (prop.CanWrite && value != null)
                {
                    PropertyInfo propInfo = data.GetType().UnderlyingSystemType.GetProperty(prop.Name);
                    if (propInfo != null)
                    {
                        if (propType.IsEnum)
                        {
                            try
                            {
                                value = System.Enum.Parse(propType, value.ToString());
                            }
                            catch
                            {
                                value = System.Enum.GetName(propType, 0);
                                value = System.Enum.Parse(propType, value.ToString());
                            }
                        }
                        else if (propType.Equals(typeof(Byte[])))
                        {
                            value = Convert.FromBase64String(value.ToString());
                        }
                        else if (propType.Equals(typeof(Boolean)))
                        {
                            value = value.ToString().ToBoolean();
                        }

                        if (propType.Equals(typeof(Guid)))
                        {
                            Guid guid = new Guid(value.ToString());
                            propInfo.SetValue(data, Convert.ChangeType(guid, propType), null);
                        }
                        else
                        {
                            propInfo.SetValue(data, Convert.ChangeType(value, propType), null);
                        }
                    }
                }
            }

            return data;
        }

        public virtual T BindDBToDB(object input)
        {
            T data = new T();

            NameValueCollection form = new NameValueCollection();

            input.GetType().GetProperties().ToList()
                .ForEach(pi => form.Add(pi.Name, pi.GetValue(input, null) == null ? "" : pi.GetValue(input, null).ToString()));

            var props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var prop in props)
            {
                var propType = prop.PropertyType;
                if (propType.IsGenericType && propType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                    propType = new NullableConverter(propType).UnderlyingType;

                object value = null;
                string requestData = form[prop.Name];
                if (requestData != null)
                {
                    if (!string.IsNullOrEmpty(requestData))
                        value = requestData;
                }
                else
                {
                    requestData = form[prop.Name];
                    if (requestData != null)
                    {
                        if (!string.IsNullOrEmpty(requestData))
                            value = requestData;
                    }
                }

                if (prop.CanWrite && value != null)
                {
                    PropertyInfo propInfo = data.GetType().UnderlyingSystemType.GetProperty(prop.Name);
                    if (propInfo != null)
                    {
                        if (propType.IsEnum)
                        {
                            try
                            {
                                value = System.Enum.Parse(propType, value.ToString());
                            }
                            catch
                            {
                                value = System.Enum.GetName(propType, 0);
                                value = System.Enum.Parse(propType, value.ToString());
                            }
                        }
                        else if (propType.Equals(typeof(Byte[])))
                        {
                            value = Convert.FromBase64String(value.ToString());
                        }
                        else if (propType.Equals(typeof(Boolean)))
                        {
                            value = value.ToString().ToBoolean();
                        }

                        if (propType.Equals(typeof(Guid)))
                        {
                            Guid guid = new Guid(value.ToString());
                            propInfo.SetValue(data, Convert.ChangeType(guid, propType), null);
                        }
                        else
                        {
                            propInfo.SetValue(data, Convert.ChangeType(value, propType), null);
                        }
                    }
                }
            }

            return data;
        }
        public virtual bool Create(T entity)
        {
            bool result = false;
            this.Save(entity, ActionType.Add, out result);
            this.dbContext.SaveChanges();
            return result;
        }

        public virtual bool Create(params T[] entities)
        {
            bool result = false;
            this.SaveRange(entities, ActionType.Add, out result);
            this.dbContext.SaveChanges();
            return result;
        }

        public virtual bool Create(IEnumerable<T> entities)
        {
            bool result = false;
            this.SaveRange(entities, ActionType.Add, out result);
            this.dbContext.SaveChanges();
            return result;
        }

        public virtual bool Delete(T entity)
        {
            bool result = false;
            this.Save(entity, ActionType.Delete, out result);
            this.dbContext.SaveChanges();
            return result;
        }

        public virtual bool Delete(params T[] entities)
        {
            bool result = false;
            this.SaveRange(entities, ActionType.Delete, out result);
            this.dbContext.SaveChanges();
            return result;
        }

        public virtual bool Edit(T entity)
        {
            bool result = false;
            this.Save(entity, ActionType.Edit, out result);
            this.dbContext.SaveChanges();
            return result;
        }

        public virtual bool Edit(params T[] entities)
        {
            bool result = false;
            this.SaveRange(entities, ActionType.Edit, out result);
            this.dbContext.SaveChanges();
            return result;
        }

    }
}
