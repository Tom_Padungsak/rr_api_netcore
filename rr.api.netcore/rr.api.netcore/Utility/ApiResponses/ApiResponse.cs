﻿using rr.api.netcore.Helpers.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.Https.Responses
{
    public class ApiResponse
    {
        public StatusResponse status { get; set; }
        public dynamic data { get; set; }
        public object token { get; set; }

        public ApiResponse(int statusCode)
        {
            status = new StatusResponse();
            status.code = statusCode;
            status.description = statusCode.ToString().ToEnum<BusinessStatusCode>().Description();
        }
        public ApiResponse()
        {
            status = new StatusResponse();
            status.code = 1000;

        }

        //public string GetDescription(int statusCode)
        //{
        //    return statusCode switch
        //    {
        //        1000 => statusCode.ToString().ToEnum<BusinessStatusCode>().Description(),
        //        _ => "",
        //    };
        //}
    }
}
