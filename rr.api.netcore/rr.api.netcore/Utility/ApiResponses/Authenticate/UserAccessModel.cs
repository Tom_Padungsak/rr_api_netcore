﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace rr.api.netcore.Https.Authenticate
{
    public class UserAccessModel
    {
        public UserAccessModel()
        {
            userRoles = new List<string>();
            childUserIds = new List<string>();
        }
        public int userId { get; set; }
        public string fullName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string username { get; set; }
        public string userType { get; set; }
        public List<string> userRoles { get; set; }
        public List<string> childUserIds { get; set; }

        [JsonIgnore]
        public string password { get; set; }
    }
}
