﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rr.api.netcore.Https.Responses
{
    public class StatusResponse
    {
        public int code { get; set; }
        public string description { get; set; }
    }
}
