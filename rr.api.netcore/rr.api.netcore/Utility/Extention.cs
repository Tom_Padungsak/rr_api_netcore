﻿using System;
using System.Globalization;

namespace rr.api.netcore.Utility
{
    public static class Extention
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToLowerFirstChar(this string input)
        {
            string newString = input;
            if (!String.IsNullOrEmpty(newString) && Char.IsUpper(newString[0]))
                newString = Char.ToLower(newString[0]) + newString.Substring(1);
            return newString;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToPascalFormat(this string input)
        {
            string newString = input;
            if (!newString.IsNullOrEmptyWhiteSpace())
            {
                var yourString = input.ToLower().Replace("_", " ");
                TextInfo info = CultureInfo.CurrentCulture.TextInfo;
                newString = info.ToTitleCase(yourString).Replace(" ", string.Empty);
            }

            return newString;
        }

        #region TO BOOLEAN
        /// <summary>
        /// Gets a boolean with specified source
        /// </summary>
        /// <param name="source">
        /// The string container 1, true, y, yes and ok no case sensitive.
        /// </param>
        /// <returns>
        /// <c>true</c> if <paramref name="source"/> is 1, true, y, yes and ok not case sensitive;
        /// Otherwise, <c>false</c>.
        /// </returns>
        public static bool ToBoolean(this string source)
        {
            if (source == null) { return false; }
            source = source.Trim().ToLower();
            if (source == "1" || source == "true" || source == "y" || source == "yes" || source == "ok")
            {
                return true;
            }

            return false;
        }
        #endregion

        /// <summary>
        /// Geta an integer with specified source.
        /// </summary>
        /// <param name="source">
        /// The string number.
        /// </param>
        /// <returns>
        /// Integer number.
        /// </returns>
        public static int ToInt32(this string source)
        {
            int result = 0;
            if (!source.IsNullOrEmptyWhiteSpace())
                result = Convert.ToInt32(source);

            return result;
        }

        /// <summary>
        /// Geta an decimal with specified source.
        /// </summary>
        /// <param name="source">
        /// The string decimal.
        /// </param>
        /// <returns>
        /// decimal number.
        /// </returns>
        public static decimal? ToDecimalDB(this string source)
        {
            decimal? result = 0;
            if (!source.IsNullOrEmptyWhiteSpace())
                result = Convert.ToDecimal(source);

            return result;
        }

        public static decimal ToDecimal(this string source)
        {
            decimal result = 0;
            if (!source.IsNullOrEmptyWhiteSpace())
                result = Convert.ToDecimal(source);

            return result;
        }

        /// <summary>
        /// Geta an decimal with specified source.
        /// </summary>
        /// <param name="source">
        /// The string decimal.
        /// </param>
        /// <returns>
        /// decimal number.
        /// </returns>
        public static bool IsNullOrEmptyWhiteSpace(this string source)
        {
            bool result = false;
            if (string.IsNullOrEmpty(source) || string.IsNullOrWhiteSpace(source))
                result = true;

            return result;
        }

        /// <summary>
        /// Geta an DateTime with specified source.
        /// </summary>
        /// <param name="source">
        /// The string DateTime.
        /// </param>
        /// <returns>
        /// DateTime number.
        /// </returns>
        public static DateTime? ToDbDateTime(this string source)
        {
            DateTime? result = null;
            if (string.IsNullOrEmpty(source) || string.IsNullOrWhiteSpace(source))
                result = Convert.ToDateTime(source);

            return result;
        }

        /// <summary>
        /// Geta an DateTime with specified source.
        /// </summary>
        /// <param name="source">
        /// The string DateTime.
        /// </param>
        /// <returns>
        /// DateTime number.
        /// </returns>
        public static DateTime ToDateTime(this string source)
        {
            DateTime result = DateTime.Now;
            if (!string.IsNullOrEmpty(source) || !string.IsNullOrWhiteSpace(source))
                result = Convert.ToDateTime(source);

            return result;
        }
    }
}
