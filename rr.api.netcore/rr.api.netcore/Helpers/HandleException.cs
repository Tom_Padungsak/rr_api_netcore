﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace rr.api.netcore.Helpers
{
    public class HandleException
    {
        public string GetEntityErrorException(Exception exception, ref int statusCode)
        {
            string resultException = string.Empty;

            var entityException = exception.GetType();
            //if (entityException == typeof(DbEntityValidationException))
            //{
            //    var entityErrors = (DbEntityValidationException)exception;
            //    var errorMessages = entityErrors.EntityValidationErrors
            //       .SelectMany(x => x.ValidationErrors)
            //       .Select(x => x.ErrorMessage);
            //    statusCode = HttpStatusCode.LengthRequired.GetHashCode();
            //    // Join the list to a single string.
            //    var fullErrorMessage = string.Join("; ", errorMessages);

            //    // Combine the original exception message with the new one.
            //    resultException = string.Concat(entityErrors.Message, " The validation errors are: ", fullErrorMessage);
            //}
            //else
            //{
            if (exception.InnerException != null)
            {
                if (exception.InnerException.InnerException != null)
                {
                    if (exception.InnerException.InnerException.InnerException != null)
                        resultException += exception.InnerException.InnerException.InnerException.Message + Environment.NewLine;
                    else
                        resultException += exception.InnerException.InnerException.Message + Environment.NewLine;
                }
                else
                {
                    resultException += exception.InnerException.Message + Environment.NewLine;
                }
            }
            else
            {
                resultException += exception.Message + Environment.NewLine;
            }
            //}
            return resultException;
        }
    }
}
