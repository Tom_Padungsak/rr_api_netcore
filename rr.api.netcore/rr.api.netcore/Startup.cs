using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using rr.api.netcore.Configurations;
using rr.api.netcore.Helpers;
using rr.api.netcore.Middlewares;
using System.Text;
using System.Threading.Tasks;

namespace rr.api.netcore
{
    public class Startup
    {
        public static IConfiguration Configuration { get; set; }
        private string[] allowedHost;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            allowedHost = Configuration.GetSection("AllowedHosts").Get<string[]>();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "RR Api Spec", Version = "v1" });

                // var basePath = AppContext.BaseDirectory;
                // var xmlPath = Path.Combine(basePath, "WebApi.xml");
                // c.IncludeXmlComments(xmlPath);
            });
      
            services.AddMvc();
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = ctx => new ValidationProblemDetailsResult();
            });
            // If using Kestrel:
            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            // If using IIS:
            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                };
                options.Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = context =>
                    {
                        if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                        {
                            context.Response.Headers.Add("Token-Expired", "true");
                        }
                        return Task.CompletedTask;
                    }
                };
            });
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader());
            });
            //services.AddCors(options => options.AddPolicy("CorsPolicy", builder =>
            //{
            //    builder.AllowAnyMethod().AllowAnyHeader()
            //            .WithOrigins(allowedHost != null ? allowedHost : new string[] { "*" })
            //            .AllowCredentials();
            //}));
            // configure DI for application services
            //services.AddScoped<IService, AbBLRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            #region Exceptions
            //            app.UseExceptionHandler(errorApp =>
            //            {
            //#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
            //                errorApp.Run(async context =>
            //#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
            //                {
            //                    var errorFeature = context.Features.Get<IExceptionHandlerFeature>();
            //                    var exception = errorFeature.Error;

            //                    //var errorDetail = context.Request.IsTrusted() ? exception.Demystify().ToString()
            //                    //   : "The instance value should be used to identify the problem when calling customer support";

            //                    //new ApiExceptionResponse(4000, exception);

            //                    var apiResponse = new ApiExceptionResponse(4000, exception);

            //                    // if (exception is BadHttpRequestException badHttpRequestException)
            //                    //{
            //                    //    apiResponse.status.code = (int)typeof(BadHttpRequestException).GetProperty("StatusCode",
            //                    //        BindingFlags.NonPublic | BindingFlags.Instance).GetValue(badHttpRequestException);
            //                    //    apiResponse.status.description = "Invalid request" + badHttpRequestException.Message;
            //                    //}
            //                    //else
            //                    //{
            //                    //    apiResponse.status.code = 500;
            //                    //    apiResponse.status.description = "An unexpected error occurred!" + exception.GetBaseException().ToString();
            //                    //}

            //                    //apiResponse.status.code = 500;
            //                    //apiResponse.status.description = "An unexpected error occurred!" + exception.GetBaseException().ToString();

            //                    // log the exception etc..

            //                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            //                    await context.Response.WriteJson(apiResponse);

            //                    // log the exception etc..
            //                    // produce some response for the caller
            //                });
            //            });
            #endregion

            app.UseAuthenticationMiddleware();

            app.UseErrorHandlingMiddleware();

            // Register any middleware to report exceptions to a third-party service *after* our ErrorHandlingMiddleware
            //app.UseExcepticon();

            //app.UseHttpsRedirection();

            app.UseRouting();

            // who are you?  
            app.UseAuthentication();

            // are you allowed?  
            app.UseAuthorization();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "RR Api Spec V1");
            });

            app.UseCors("CorsPolicy");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
